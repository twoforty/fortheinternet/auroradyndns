<#
Please not that this code makes use of Posh-AuroraDNS.
See https://github.com/j81blog/Posh-AuroraDNS for more information.
#>

# Import AuroraDNS functions.
. ./aurora-dns.ps1

function Write-Log {
    <#
    .SYNOPSIS
    Write-Output where output is prefixed with a timestamp.
    #>
    [CmdletBinding()]
    param (
        [string]$Message
    )
    $dt = (Get-Date).ToString("yyyy-MM-dd hh:mm:ss")
    Write-Output "$dt $Message"
}

# Parse the list of host records to process.
$hosts = @(($env:DNS_HOSTS).Split(','))
if (-not ($hosts.Count -ge 1)) {
    throw "No hosts specified in DNS_HOSTS"
}

# Setup AuroraDNS authentication.
$auroraAuthorization = @{ Api=$env:AURORA_API; Key=$env:AURORA_KEY; Secret=$env:AURORA_SECRET }

# Get zone from AuroraDNS.
$zone = Invoke-AuroraGetZones @auroraAuthorization | Where-Object { $_.name -eq $ENV:AURORA_ZONE }

# Forever.
while($true) {
    try {
        # Get public IP.
        $externalIP = (Invoke-RestMethod -Uri $ENV:MYIP_URL).ip
        Write-Log -Message "ExternalIP: $externalIP"

        # Get record from AuroraDNS.
        $records = Invoke-AuroraGetRecord -ZoneID $zone.id -RecordName $hostname @auroraAuthorization
        foreach($hostname in $hosts) {
            if ($hostname -eq "@") {
                # Select empty host or root domain record
                $record = $records | Where-Object { $_.name -eq "" }
            } else {
                # Select named host
                $record = $records | Where-Object { $_.name -eq $hostname }
            }
            Write-Log "$($record.name): DNS record of type [$($record.type)] has content [$($record.content)]"

            if ($externalIP -ne $record.content) {
                # DNS record has different content than the public IP. Update the DNS record.
                # NB: Updating the DNS record will change it to an A-record.
                Write-Log "$($record.name): DNS record IP [$($record.content)] differs from external IP [$externalIP] and will updated"
                $record = Invoke-AuroraSetRecord -ZoneID $zone.id -RecordID $record.id -Type "A" -Content $externalIP @auroraAuthorization
            }
        }
    } catch {
        Write-Log "Error: $($_.Exception.Message)"
    }

    Start-Sleep -Seconds $ENV:INTERVAL
}

<#
$zone looks like:
account_id : 81c5fc4b-52f1-4053-b9cb-b9eb69bff022
cluster_id : 8f48fabe-57c7-431f-8164-58cff104f6ca
created    : 15/01/2019 14:19:51
id         : 05fc8e32-18e4-428f-938d-4e9b935133cb
name       : vosenterprises.eu
servers    : {ns091.auroradns.eu, ns092.auroradns.nl, ns093.auroradns.info}
#>

<#
A-record looks like this:
content         : 95.96.214.246
created         : 09/03/2018 11:09:25
disabled        : False
health_check_id :
id              : 9143fd66-faad-4f00-b434-39dfeb7a6cec
modified        : 10/03/2024 20:21:14
name            : gitlab
prio            : 0
ttl             : 3600
type            : A
#>

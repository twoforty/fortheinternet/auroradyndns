<#
All code in this file is made by j81blog: https://github.com/j81blog
All code in this file is taken from Posh-AuroraDNS-2021.0530.1330
The original module cannot be used, since it reports that it cannot be run on Linux.

Following is the original license for Posh-AuroraDNS-2021.0530.1330:
MIT License

Copyright (c) 2021 John B.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


#>
function Get-AuroraDNSAuthorizationHeader {
    <#
.SYNOPSIS
    Create headers required for Aurora DNS authorization
.DESCRIPTION
    Create headers required for Aurora DNS authorization
.PARAMETER Key
    The Aurora DNS API key for your account.
.PARAMETER Secret
    The Aurora DNS API secret key for your account.
.PARAMETER Method
    The method used for this action.
    Some of the most used: 'POST', 'GET' or 'DELETE'
.PARAMETER Uri
    The Uri used for this action.
    Example: '/zones'
.PARAMETER ContentType
    The content type.
    Default value (if not specified): 'application/json; charset=UTF-8'
.EXAMPLE
    $authorizationHeader = Get-AuroraDNSAuthorizationHeader -Key XXXXXXXXXX -Secret YYYYYYYYYYYYYYYY -Method GET -Uri /zones
.NOTES
    Function Name : Invoke-AuroraFindZone
    Version       : v2021.0530.1330
    Author        : John Billekens
    Requires      : API Account => https://cp.pcextreme.nl/auroradns/users
.LINK
    https://github.com/j81blog/Posh-AuroraDNS
#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [String]$Key,

        [Parameter(Mandatory)]
        [String]$Secret,

        [Parameter(Mandatory)]
        [String]$Method,

        [Parameter(Mandatory)]
        [String]$Uri,

        [Parameter()]
        [String]$ContentType = "application/json; charset=UTF-8",

        [Parameter(DontShow)]
        [String]$TimeStamp = $((get-date).ToUniversalTime().ToString("yyyyMMddTHHmmssZ")),

        [Parameter(ValueFromRemainingArguments, DontShow)]
        $ExtraParams
    )
    $Message = '{0}{1}{2}' -f $Method, $Uri, $TimeStamp
    $hmac = New-Object System.Security.Cryptography.HMACSHA256
    $hmac.key = [Text.Encoding]::UTF8.GetBytes($Secret)
    $Signature = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($Message))
    $SignatureB64 = [Convert]::ToBase64String($signature)
    $AuthorizationString = '{0}:{1}' -f $Key, $SignatureB64
    $Authorization = [Text.Encoding]::UTF8.GetBytes($AuthorizationString)
    $AuthorizationB64 = [Convert]::ToBase64String($Authorization)

    $headers = @{
        'X-AuroraDNS-Date' = $TimeStamp
        'Authorization'    = $('AuroraDNSv1 {0}' -f $AuthorizationB64)
        'Content-Type'     = $ContentType
    }
    Write-Output $headers
}

function Invoke-AuroraGetZones {
    <#
.SYNOPSIS
    Get Aurora DNS Zones
.DESCRIPTION
    Get Aurora DNS Zones
.PARAMETER Key
    The Aurora DNS API key for your account.
.PARAMETER Secret
    The Aurora DNS API secret key for your account.
.PARAMETER Api
    The Aurora DNS API hostname.
    Default (if not specified): api.auroradns.eu
.EXAMPLE
    $auroraAuthorization = @{ Api='api.auroradns.eu'; Key='XXXXXXXXXX'; Secret='YYYYYYYYYYYYYYYY' }
    PS C:\>$zones = Invoke-AuroraGetZones @auroraAuthorization
.NOTES
    Function Name : Invoke-AuroraGetZones
    Version       : v2021.0530.1330
    Author        : John Billekens
    Requires      : API Account => https://cp.pcextreme.nl/auroradns/users
.LINK
    https://github.com/j81blog/Posh-AuroraDNS
#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Key,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Secret,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [String]$Api = 'api.auroradns.eu',

        [Parameter(ValueFromRemainingArguments, DontShow)]
        $ExtraParams
    )
    $UseBasic = @{ }
    if ('UseBasicParsing' -in (Get-Command Invoke-RestMethod).Parameters.Keys) {
        $UseBasic.UseBasicParsing = $true
    }
    $Method = 'GET'
    $Uri = '/zones'
    $ApiUrl = 'https://{0}{1}' -f $Api, $Uri
    $AuthorizationHeader = Get-AuroraDNSAuthorizationHeader -Key $Key -Secret $Secret -Method $Method -Uri $Uri
    $restError = ''
    Write-Debug "$Method URI: `"$ApiUrl`""
    try {
        [Object[]]$result = Invoke-RestMethod -Uri $ApiUrl -Headers $AuthorizationHeader -Method $Method -ErrorVariable restError @UseBasic
    } catch {
        $result = $null
        $OutError = $restError[0].Message | ConvertFrom-Json -ErrorAction SilentlyContinue
        Write-Debug $($OutError | Out-String)
        Throw ($OutError.errormsg)
    }
    if ( ($result.Count -gt 0) -and ($null -ne $result[0].id) -and (-not [String]::IsNullOrEmpty($($result[0].id))) ) {
        Write-Output $result
    } else {
        Write-Debug "The function generated no data"
        Write-Output $null
    }
}

function Invoke-AuroraGetRecord {
    <#
.SYNOPSIS
    Get Aurora DNS Record
.DESCRIPTION
    Get Aurora DNS Record
.PARAMETER Key
    The Aurora DNS API key for your account.
.PARAMETER Secret
    The Aurora DNS API secret key for your account.
.PARAMETER RecordID
    Specify a specific Aurora DNS Record ID (GUID).
.PARAMETER RecordName
    Specify a specific Aurora DNS Record Name (String).
.PARAMETER ZoneID
    Specify a specific Aurora DNS Zone ID (GUID).
.PARAMETER Api
    The Aurora DNS API hostname.
    Default (if not specified): api.auroradns.eu
.EXAMPLE
    $auroraAuthorization = @{ Api='api.auroradns.eu'; Key='XXXXXXXXXX'; Secret='YYYYYYYYYYYYYYYY' }
    PS C:\>$record = Invoke-AuroraGetRecord -ZoneID 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee' @auroraAuthorization
    List all records by not specifying 'RecordID' or 'RecordName' with a value
.EXAMPLE
    $auroraAuthorization = @{ Api='api.auroradns.eu'; Key='XXXXXXXXXX'; Secret='YYYYYYYYYYYYYYYY' }
    PS C:\>$record = Invoke-AuroraGetRecord -ZoneID 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee' -RecordName 'www' @auroraAuthorization
    Get record with name 'www' in zone 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee'
.EXAMPLE
    $auroraAuthorization = @{ Api='api.auroradns.eu'; Key='XXXXXXXXXX'; Secret='YYYYYYYYYYYYYYYY' }
    PS C:\>$record = Invoke-AuroraGetRecord -ZoneID 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee' -RecordID 'vvvvvvvv-wwww-xxxx-yyyy-zzzzzzzzzzzz' @auroraAuthorization
    Get record with ID 'vvvvvvvv-wwww-xxxx-yyyy-zzzzzzzzzzzz' in zone 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee'
.NOTES
    Function Name : Invoke-AuroraGetRecord
    Version       : v2021.0530.1330
    Author        : John Billekens
    Requires      : API Account => https://cp.pcextreme.nl/auroradns/users
.LINK
    https://github.com/j81blog/Posh-AuroraDNS
#>
    [CmdletBinding(DefaultParameterSetName = 'All')]
    param(
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Key,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Secret,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [GUID[]]$ZoneID,

        [Parameter(ParameterSetName = 'GUID', Mandatory)]
        [GUID[]]$RecordID,

        [Parameter(ParameterSetName = 'Named')]
        [String]$RecordName,

        [Parameter(ParameterSetName = 'Named')]
        [String]$Co,

        [Parameter()]
        [String]$Api = 'api.auroradns.eu',

        [Parameter(ValueFromRemainingArguments, DontShow)]
        $ExtraParams
    )
    $UseBasic = @{ }
    if ('UseBasicParsing' -in (Get-Command Invoke-RestMethod).Parameters.Keys) {
        $UseBasic.UseBasicParsing = $true
    }
    $Method = 'GET'
    if ($PSCmdlet.ParameterSetName -like "GUID") {
        $Uri = '/zones/{0}/records/{1}' -f $ZoneID.Guid, $RecordID.Guid
    } else {
        $Uri = '/zones/{0}/records' -f $ZoneID.Guid
    }
    Write-Verbose "$Uri"
    $ApiUrl = 'https://{0}{1}' -f $Api, $Uri
    $AuthorizationHeader = Get-AuroraDNSAuthorizationHeader -Key $Key -Secret $Secret -Method $Method -Uri $Uri
    $restError = ''
    try {
        Write-Debug "$Method URI: `"$ApiUrl`""
        [Object[]]$result = Invoke-RestMethod -Uri $ApiUrl -Headers $AuthorizationHeader -Method $Method -ErrorVariable restError @UseBasic
        if ($PSBoundParameters.ContainsKey('distributionalgorithm')) { $Payload.Add('distributionalgorithm', $distributionalgorithm) }

        if ($PSCmdlet.ParameterSetName -like "Named") {
            [Object[]]$result = $result | Where-Object { $_.name -eq $RecordName }
        }
    } catch {
        $result = $null
        $OutError = $restError[0].Message | ConvertFrom-Json -ErrorAction SilentlyContinue
        Write-Debug $($OutError | Out-String)
        if ($OutError.error -eq 'NoSuchRecordError') {
            $result = $null
        } else {
            Throw ($OutError.errormsg)
        }
    }
    if ( ($result.Count -gt 0) -and ($null -ne $result[0].id) -and (-not [String]::IsNullOrEmpty($($result[0].id))) ) {
        Write-Output $result
    } else {
        Write-Debug "The function generated no data"
        Write-Output $null
    }
}

function Invoke-AuroraSetRecord {
    <#
.SYNOPSIS
    Set Aurora DNS Record with new values
.DESCRIPTION
    Get Aurora DNS Record with new values
.PARAMETER Key
    The Aurora DNS API key for your account.
.PARAMETER Secret
    The Aurora DNS API secret key for your account.
.PARAMETER ZoneID
    Specify a specific Aurora DNS Zone ID (GUID).
.PARAMETER RecordID
    Specify a specific Aurora DNS Record ID (GUID).
.PARAMETER Name
    Specify a name fo the new record.
.PARAMETER Content
    Specify the content for the nwe record.
.PARAMETER TTL
    Specify a Time To Live value in seconds.
    Default (if not specified): 3600
.PARAMETER Type
    Specify the record type.
    Can contain one of the following values: "A", "AAAA", "CNAME", "MX", "NS", "SOA", "SRV", "TXT", "DS", "PTR", "SSHFP", "TLSA"
    Default (if not specified): "A"
.PARAMETER Api
    The Aurora DNS API hostname.
    Default (if not specified): api.auroradns.eu
.EXAMPLE
    $auroraAuthorization = @{ Api='api.auroradns.eu'; Key='XXXXXXXXXX'; Secret='YYYYYYYYYYYYYYYY' }
    PS C:\>$record = Invoke-AuroraSetRecord -ZoneID 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee' -RecordID 'vvvvvvvv-wwww-xxxx-yyyy-zzzzzzzzzzzz' -Content 198.51.100.85 @auroraAuthorization
    Set an existing record with new content '198.51.100.85'
.NOTES
    Function Name : Invoke-AuroraAddRecord
    Version       : v2021.0530.1330
    Author        : John Billekens
    Requires      : API Account => https://cp.pcextreme.nl/auroradns/users
.LINK
    https://github.com/j81blog/Posh-AuroraDNS
#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [string]$Key,

        [Parameter(Mandatory)]
        [string]$Secret,

        [Parameter(Mandatory)]
        [GUID[]]$ZoneID,

        [Parameter(Mandatory)]
        [GUID[]]$RecordId,

        [String]$Name,

        [String]$Content,

        [int]$TTL = 3600,

        [ValidateSet('A', 'AAAA', 'CNAME', 'MX', 'NS', 'SOA', 'SRV', 'TXT', 'DS', 'PTR', 'SSHFP', 'TLSA')]
        [String]$Type = 'A',

        [Parameter()]
        [String]$Api = 'api.auroradns.eu',

        [Switch]$PassThru,

        [Parameter(ValueFromRemainingArguments, DontShow)]
        $ExtraParams
    )
    $UseBasic = @{ }
    if ('UseBasicParsing' -in (Get-Command Invoke-RestMethod).Parameters.Keys) {
        $UseBasic.UseBasicParsing = $true
    }
    $Method = 'PUT'
    $Uri = '/zones/{0}/records/{1}' -f $ZoneId.Guid, $RecordId.Guid
    $ApiUrl = 'https://{0}{1}' -f $Api, $Uri
    $AuthorizationHeader = Get-AuroraDNSAuthorizationHeader -Key $Key -Secret $Secret -Method $Method -Uri $Uri
    $restError = ''

    $Payload = @{ }
    if ($PSBoundParameters.ContainsKey('Name')) { $Payload.Add('name', $Name) }
    if ($PSBoundParameters.ContainsKey('TTL')) { $Payload.Add('ttl', $TTL) }
    if ($PSBoundParameters.ContainsKey('Type')) { $Payload.Add('type', $Type) }
    if ($PSBoundParameters.ContainsKey('Content')) { $Payload.Add('content', $Content) }

    $Body = $Payload | ConvertTo-Json

    Write-Debug "$Method URI: `"$ApiUrl`""
    try {
        [Object[]]$result = Invoke-RestMethod -Uri $ApiUrl -Headers $AuthorizationHeader -Method $Method -Body $Body -ErrorVariable restError @UseBasic
        if ($PassThru -and (($result.Count -eq 0) -or ([string]::IsNullOrWhiteSpace($result)))) {
            [Object[]]$result = Invoke-AuroraGetRecord -ZoneID $ZoneID -RecordID $RecordID -Key $Key -Secret $Secret -Api $Api
        }
    } catch {
        $result = $null
        $OutError = $restError[0].Message | ConvertFrom-Json -ErrorAction SilentlyContinue
        Write-Debug $($OutError | Out-String)
        Throw ($OutError.errormsg)
    }
    if ( ($result.Count -gt 0) -and ($null -ne $result[0].id) -and (-not [String]::IsNullOrEmpty($($result[0].id))) ) {
        Write-Output $result
    } else {
        Write-Debug "The function generated no data"
        Write-Output $null
    }
}

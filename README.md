# AuroraDynDNS

Creates a Docker image that provides a Dynamic DNS function when your DNS domain is hosted on Aurora DNS (PCExtreme / Versio).

## Getting started

Run the container in your own network so that it can detect you public IP.

Configure the following ENV variables:

- `AURORA_KEY`

Set your AuroraKey. See [Aurora credential](#aurora-credential)

- `AURORA_SECRET`

Set your AuroraSecret. See [Aurora credential](#aurora-credential)

- `AURORA_ZONE`

Specify the DNS zone name for which to perform Dynamic DNS. Only one Domain Name can be specified.

- `DNS_HOSTS`

Comma separated list of hostnames to monitor for DynDNS.
Specify the root domain record via `@`.
Note that hostnames listed here will be updated to type A-record.

- `INTERVAL`

Interval in seconds to check the DNS records in `DNS_HOSTS`.

- `MYIP_URL`

Url that points to the myip API. Default=`https://api.myip.com`
You probably do not need to change this.

- `AURORA_API`

Hostname for the Aurora DNS API. Default=api.auroradns.eu
You probably do not need to change this.

## Aurora credential

Generate API Credentials for your account from the DNS - [Health Checks Users](https://cp.pcextreme.nl/auroradns/users) page in Aurora. You should end up with an API URL, Key and Secret value.

## Example

```bash
MYIP_URL=https://api.myip.com
AURORA_API=api.auroradns.eu
AURORA_KEY=ChangeME
AURORA_SECRET=ChangeME
AURORA_ZONE=mydomain.tld
DNS_HOSTS=@,www,rdp
INTERVAL=60
```

## MIT License

Copyright (c) 2024 Rodric Vos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Posh-AuroraDNS license

Following is the original license for [Posh-AuroraDNS](https://github.com/j81blog/Posh-AuroraDNS):
MIT License

Copyright (c) 2021 John B.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

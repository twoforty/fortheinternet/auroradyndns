# syntax=docker/dockerfile:1

FROM mcr.microsoft.com/powershell
ENV MYIP_URL=https://api.myip.com
ENV AURORA_API=api.auroradns.eu
ENV AURORA_KEY=change_me
ENV AURORA_SECRET=change_me
ENV AURORA_ZONE=domain.tld
ENV DNS_HOSTS=www,site
ENV INTERVAL=60

WORKDIR /app
COPY src/ /app

# Posh-AuroraDNS gives an error when running on Linux.
# RUN \
# apt-get update && \
# apt-get install wget unzip -y && \
# wget https://github.com/j81blog/Posh-AuroraDNS/archive/refs/tags/v2021.0530.1330.zip && \
# unzip v2021.0530.1330.zip  && \
# rm v2021.0530.1330.zip && \
# rm -rf /var/lib/apt/lists/*

CMD ["pwsh", "/app/dyndns.ps1"]
